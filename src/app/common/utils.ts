export const formatDate = (date: string): string => {
  const parsed = new Date(date);
  const currentMonth = parsed.getMonth() + 1;
  const currentDay = parsed.getDate();
  const MONTH = currentMonth < 10 ? `0${currentMonth}` : currentMonth;
  const DAY = currentDay < 10 ? `0${currentDay}` : currentDay;

  return `${parsed.getFullYear()}-${MONTH}-${DAY}`;
}

export const formatCurrency = (money:number) => {
  return new Intl.NumberFormat(
    window.navigator.language,
    { style: 'currency', currency: 'EUR' }
  ).format(money);
}
