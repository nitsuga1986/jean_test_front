import { useApi } from 'api';
import { Paths } from 'api/gen/client';
import React, { useState, useEffect, useCallback } from 'react'
import { Alert } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'app/ui/components/Button';

interface IAppContext {
  customers?: Paths.GetSearchCustomers.Responses.$200;
  products?: Paths.GetSearchProducts.Responses.$200;
  invoices?: Paths.GetInvoices.Responses.$200,
  updateInvoices: any;
  showToast: (message: string, error?: boolean) => void;
}

export const AppContext = React.createContext<IAppContext>({
  customers: undefined,
  products: undefined,
  invoices: undefined,
  updateInvoices: undefined,
  showToast: () => { }
})

const AppProvider = ({ children }: any) => {
  const api = useApi();

  const [state, setState] = useState<Omit<IAppContext, "updateInvoices" | "showToast">>({
    customers: undefined,
    products: undefined,
    invoices: undefined,
  })

  const [toast, setToast] = useState({
    show: false,
    message: '',
    error: false
  })

  const fetchData = useCallback(async () => {

    const { data: Invoices } = await api.getInvoices();
    const { data: Products } = await api.getSearchProducts({ query: '' });
    const { data: Customers } = await api.getSearchCustomers({ query: '' });

    setState({
      ...state,
      customers: Customers,
      products: Products,
      invoices: Invoices
    })

  }, [api]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);


  const updateInvoices = async () => {
    const { data: Invoices } = await api.getInvoices();
    setState({
      ...state,
      invoices: Invoices
    })
  }

  const showToast = (message: string, error: boolean = false) => {
    setToast({
      show: true,
      message,
      error
    })

    setTimeout(() => {
      setToast({
        show: false,
        message: '',
        error: false
      })
    }, 3000)
  }


  return (
    <AppContext.Provider value={{ customers: state.customers, products: state.products, invoices: state.invoices, updateInvoices, showToast }}>
      {toast.show && (
        <Alert key={'idx'} variant={toast.error ? "danger" : "primary"} style={{ maxWidth: '50vw', position: 'fixed', bottom: '1rem', right: '1rem' }}>
          {toast.message}
          <hr />
          <div className="d-flex justify-content-end">
            <Button onClick={() => setToast({ show: false, message: '', error: false })} secondary text="Close" />
          </div>
        </Alert>
      )}

      {children}
    </AppContext.Provider>
  )
}

export default AppProvider;