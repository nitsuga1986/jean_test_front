import { Customers } from "app/views/Customers";
import { Products } from "app/views/Products";
import { Invoices } from "app/views/Invoices";
import { CreateInvoice } from "app/views/CreateInvoice";
import { UpdateInvoice } from "app/views/UpdateInvoice";
import { IRoute } from "types";



export const routes: Array<IRoute> = [
  {
    key: 'customers',
    view: Customers,
    path: '/customers'
  },
  {
    key: 'products',
    view: Products,
    path: '/products'
  },
  {
    key: 'invoices',
    view: Invoices,
    path: '/'
  },
  {
    key: 'invoices',
    view: Invoices,
    path: '/invoices'
  },
  {
    key: 'invoices-create',
    view: CreateInvoice,
    path: '/invoices/create'
  },
  {
    key: 'invoices-update',
    view: UpdateInvoice,
    path: '/invoices/update/:id'
  }
];
