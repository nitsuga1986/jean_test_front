import { useApi } from 'api';
import { useHistory } from "react-router-dom";
import { AppContext } from 'app/common/contexts/AppContext';
import { Table } from 'app/ui/components/Table';
import { Title } from 'app/ui/components/Title';
import React, { useState, useContext, useCallback, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faThumbsDown, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { Button } from 'app/ui/components/Button';
import { Modal } from 'react-bootstrap';
import { formatCurrency } from 'app/common/utils';

export const Invoices: React.FC<{}> = (props) => {
  let history = useHistory();
  const api = useApi();
  const { invoices, updateInvoices, showToast } = useContext(AppContext)
  const [invoicesList, setInvoicesList] = useState(invoices);
  const [showModal, setShowModal] = useState({ visible: false, id: 0 });

  useEffect(() => { setInvoicesList(invoices) }, [invoices]);

  const onDeleteInvoice = async (id: number) => {
    try {
      await api.deleteInvoice({ id });
      updateInvoices();
      showToast('Invoice delete successfully')
    } catch (err: any) {
      const { message } = err.response.data;
      showToast(message, true)
    }
  }

  const onUpdateInvoice = (id: number) => {
    history.push(`/invoices/update/${id}`)
  }

  const fetchPageInvoices = useCallback(async (page: number) => {
    const { data } = await api.getInvoices({ page });
    setInvoicesList(data);
    window.scrollTo({
      top: 100,
      behavior: 'smooth'
    });
  }, [api]);

  const columns = [
    {
      Header: 'Number',
      accessor: 'id'
    },
    {
      Header: 'Customer',
      accessor: 'customer',
      Cell: (props: any) => {
        const value = props.cell.row.original.customer;
        return <>{value.first_name} {value.last_name}</>
      },
    },
    {
      Header: 'Date',
      accessor: 'date',
    },
    {
      Header: 'Deadline',
      accessor: 'deadline',
    },
    {
      Header: 'Finalized',
      accessor: 'finalized',
      Cell: (props: any) => {
        const isFinalized = props.cell.row.original.finalized;
        return <>{isFinalized ? <FontAwesomeIcon color="#1DAD4A" fixedWidth icon={faThumbsUp} /> : <FontAwesomeIcon color="#FF5556" fixedWidth icon={faThumbsDown} />}</>
      }
    },
    {
      Header: 'Paid',
      accessor: 'paid',
      Cell: (props: any) => {
        const isPaid = props.cell.row.original.paid;
        return <>{isPaid ? <FontAwesomeIcon color="#1DAD4A" fixedWidth icon={faThumbsUp} /> : <FontAwesomeIcon fixedWidth color="#FF5556" icon={faThumbsDown} />}</>
      }
    },
    {
      Header: 'Tax Base',
      accessor: 'tax_base',
      Cell: (props: any) => {
        const { total, tax } = props.cell.row.original;
        return <>{formatCurrency(parseFloat(total) - parseFloat(tax))}</>
      }
    },
    {
      Header: 'Tax',
      accessor: 'tax',
      Cell: (props: any) => {
        const { tax } = props.cell.row.original;
        return <>{formatCurrency(tax)}</>
      }
    },
    {
      Header: 'Total',
      accessor: 'total',
      Cell: (props: any) => {
        const { total } = props.cell.row.original;

        return <>{formatCurrency(total)}</>
      }
    },
    {
      Header: 'Options',
      accessor: 'options',
      Cell: (props: any) => {

        const { id } = props.row.original;

        return (
          <>
            <Button noMargin small secondary text={<FontAwesomeIcon color="#28a745" fixedWidth icon={faEdit} onClick={() => { onUpdateInvoice(id) }} />} onClick={() => { }} />
            <Button noMargin small secondary text={<FontAwesomeIcon color="#FF5556" fixedWidth icon={faTrashAlt} onClick={() => { setShowModal({ visible: true, id }) }} />} onClick={() => { }} />

          </>
        )
      }
    }
  ]
  return (
    <>
      <Title title="Invoices" amount={invoicesList?.pagination.total_entries || 0} buttons={[<Button text='New Invoice' onClick={() => { history.push('/invoices/create') }} />]} />


      {invoicesList && (
        <Table
          pagination={invoicesList?.pagination}
          data={invoicesList?.invoices}
          columns={columns}
          onClickPage={(page) => fetchPageInvoices(page)}
        />
      )}

      <Modal show={showModal.visible} >
        <Modal.Header closeButton>
          <Modal.Title>Delete invoice</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this invoice?</Modal.Body>
        <Modal.Footer>
          <Button secondary onClick={() => setShowModal({ visible: false, id: 0 })} text="No" />
          <Button onClick={() => { setShowModal({ visible: false, id: 0 }); onDeleteInvoice(showModal.id) }} text="Yes" />
        </Modal.Footer>
      </Modal>
    </>
  )
}