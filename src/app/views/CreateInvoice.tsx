import { useApi } from "api";
import { useContext } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom'
import { AppContext } from "app/common/contexts/AppContext";
import { Components } from 'api/gen/client';
import { InvoiceForm, } from "app/ui/components/InvoiceForm";

interface IInvoiceCreatePayload {
  invoice: Components.Schemas.InvoiceCreatePayload;
}

export const CreateInvoice = () => {
  const api = useApi();
  let history = useHistory();
  const { updateInvoices, showToast } = useContext(AppContext)

  const onCreateInvoice = async (requestInvoice: IInvoiceCreatePayload) => {
    try {
      await api.postInvoices({}, requestInvoice)
      updateInvoices();
      history.push('/invoices')
      showToast(`Invoice created successfully`);
    } catch (err: any) {
      const { message } = err.response.data;
      showToast(message ?? 'There was an error on update', true);
    }

  }
  return (
    <InvoiceForm
      initialValues={
        {
          customer_id: 0,
          customer: '',
          deadline: '',
          date: '',
          finalized: false,
          paid: false,
          invoice_lines: []
        }
      }
      isCreating
      onSendData={(value) => onCreateInvoice(value as IInvoiceCreatePayload)}
    />


  );
}