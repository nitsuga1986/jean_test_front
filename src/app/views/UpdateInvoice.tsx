import { useApi } from "api";
import { useContext, useEffect, useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from 'react-router-dom'
import { AppContext } from "app/common/contexts/AppContext";
import { Components } from 'api/gen/client';
import { InvoiceForm } from "app/ui/components/InvoiceForm";


interface IData extends Components.Schemas.Invoice {
  customer: Components.Schemas.Customer;
}

interface IInvoiceUpdatePayload {
  invoice: Components.Schemas.InvoiceUpdatePayload;
}


export const UpdateInvoice = ({ match }: any) => {
  const api = useApi();
  const { id } = match.params;
  const history = useHistory();

  const { updateInvoices, showToast } = useContext(AppContext)
  const [data, setData] = useState<IData>();

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await api.getInvoice({ id })

      setData(data as IData)
    }
    fetchData();

  }, [id]);

  const onUpdateInvoice = async (requestInvoice: IInvoiceUpdatePayload) => {
    try {
      data && await api.putInvoice({ id: data.id }, requestInvoice);
      updateInvoices();
      history.push('/invoices')
      showToast(`Invoice updated successfully`);
    } catch (err: any) {
      const { message } = err.response.data;
      showToast(message ?? 'There was an error on update', true);
    }

  }

  return (
    <>
      {!!data && (
        <InvoiceForm
          dataInvoice={{
            idInvoice: data.id,
            tax: data.tax,
            total: data.total
          }}
          initialValues={
            {
              customer: data.customer,
              customer_id: (data && data.customer_id) || 0,
              deadline: data.deadline || '',
              date: data.date || '',
              finalized: data.finalized,
              paid: data.paid,
              invoice_lines: data.invoice_lines
            }
          }
          isUpdating
          onSendData={(value) => onUpdateInvoice(value as IInvoiceUpdatePayload)}
        />

      )}
    </>
  );
}