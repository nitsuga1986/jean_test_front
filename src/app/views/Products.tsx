
import { useApi } from 'api';
import { AppContext } from 'app/common/contexts/AppContext';
import { formatCurrency } from 'app/common/utils';
import { Search } from 'app/ui/components/Search';
import { Table } from 'app/ui/components/Table';
import { Title } from 'app/ui/components/Title';
import React, { useState, useContext, useCallback, useEffect } from 'react';



export const Products: React.FC = (props) => {
  console.log(props)
  const api = useApi();
  const { products } = useContext(AppContext)
  console.log('products:', products)
  const [productList, setProductList] = useState(products);
  console.log(products)
  useEffect(() => { setProductList(products) }, [products]);

  const fetchPageProducts = useCallback(async (filter: string, page: number) => {
    const { data } = await api.getSearchProducts({ query: filter, page });
    setProductList(data);
    window.scrollTo({
      top: 100,
      behavior: 'smooth'
    });
  }, [api]);

  const columns = [
    {
      Header: 'Label',
      accessor: 'label',
    },
    {
      Header: 'Unit',
      accessor: 'unit',
    },
    {
      Header: 'Unit Price',
      accessor: 'unit_price',
      Cell: (props: any) => {
        const { unit_price } = props.cell.row.original;
        return <>{formatCurrency(unit_price)}</>
      }
    },
    {
      Header: 'UP Without Tax',
      accessor: 'unit_price_without_tax',
      Cell: (props: any) => {
        const { unit_price_without_tax } = props.cell.row.original;
        return <>{formatCurrency(unit_price_without_tax)}</>
      }
    },
    {
      Header: 'Unit Tax',
      accessor: 'unit_tax',
      Cell: (props: any) => {
        const { unit_tax } = props.cell.row.original;
        return <>{formatCurrency(unit_tax)}</>
      }
    },
    {
      Header: 'Vat Rate',
      accessor: 'vat_rate',
      Cell: (props: any) => {
        const { vat_rate } = props.cell.row.original;
        return <>{`${vat_rate} %`}</>
      }
    }
  ]
  return (
    <>
      <Title title="Products" amount={productList?.pagination.total_entries || 0} />
      <Search onFilter={(filter: string) => fetchPageProducts(filter, 1)} />
      {productList && (
        <Table
          pagination={productList?.pagination}
          data={productList?.products}
          columns={columns}
          onClickPage={(page) => fetchPageProducts('', page)}
        />
      )}
    </>
  )
}