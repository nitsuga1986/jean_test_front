import { useApi } from 'api';
import { AppContext } from 'app/common/contexts/AppContext';
import { Search } from 'app/ui/components/Search';
import { Table } from 'app/ui/components/Table';
import { Title } from 'app/ui/components/Title';
import React, { useState, useContext, useCallback, useEffect } from 'react';


export const Customers: React.FC<{}> = (props) => {
  const api = useApi();
  const { customers } = useContext(AppContext)
  const [customersList, setCustomersList] = useState(customers);

  useEffect(() => { setCustomersList(customers) }, [customers]);

  const fetchPageCustomers = useCallback(async (filter: string, page: number) => {
    const { data } = await api.getSearchCustomers({ query: filter, page });
    setCustomersList(data);
    window.scrollTo({
      top: 100,
      behavior: 'smooth'
    });
  }, [api]);

  const columns = [
    {
      Header: 'Full Name',
      accessor: 'first_name',
      Cell: (props: any) => {
        const value = props.cell.row.original;
        return <>{value.first_name} {value.last_name}</>
      },
    },
    {
      Header: 'Address',
      accessor: 'address',
      Cell: (props: any) => {
        const value = props.cell.row.original;
        return <>{value.address} - {value.zip_code}</>
      },
    },
    {
      Header: 'City',
      accessor: 'city',
    },
    {
      Header: 'Country',
      accessor: 'country',
      Cell: (props: any) => {
        const value = props.cell.row.original;
        return <>{value.country} ({value.country_code})</>
      },
    }
  ]
  return (
    <>
      <Title title="Customers" amount={customersList?.pagination.total_entries || 0} />
      <Search onFilter={(filter: string) => fetchPageCustomers(filter, 1)} />
      {customersList && (
        <Table
          pagination={customersList?.pagination}
          data={customersList?.customers}
          columns={columns}
          onClickPage={(page) => fetchPageCustomers('', page)}
        />
      )}
    </>
  )
}