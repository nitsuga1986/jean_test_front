import { render } from '@testing-library/react';
import { Products } from '../Products'
import AppProvider from 'app/common/contexts/AppContext';
import { ApiProvider } from 'api';
import { Router } from 'react-router';
import { MainLayout } from 'app/ui/layout/main.layout';

import { createMemoryHistory } from 'history'
const history = createMemoryHistory()

test('Should render a Main Layout', () => {

  const { debug } = render(
    <ApiProvider
      url="https://jean-test-api.herokuapp.com/"
      token="c214a5d7-e93e-430b-9d72-17ea2263260f"
    >
      <AppProvider value={{ products: [] }}>
        <Router history={history}>
          <MainLayout>

            <Products />


          </MainLayout>

        </Router>
      </AppProvider>


    </ApiProvider >
  );
  debug()
});
