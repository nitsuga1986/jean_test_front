import { AppContext } from 'app/common/contexts/AppContext';
import { useContext } from 'react';
import { Sidenav } from '../components/Sidenav';
import './styles.css';

interface IProps {
  children: React.ReactNode;
}

export const MainLayout = ({ children }: IProps) => {
  const { customers, invoices, products } = useContext(AppContext);
  return (
    <div className="main-container">
      <Sidenav
        customers_amount={customers?.pagination.total_entries || 0}
        products_amount={products?.pagination.total_entries || 0}
        invoices_amount={invoices?.pagination.total_entries || 0}
      />
      <main>{children}</main>
    </div>

  );
}