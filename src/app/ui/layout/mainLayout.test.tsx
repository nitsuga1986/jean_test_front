import { render } from '@testing-library/react';
import { MainLayout } from './main.layout';
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'

const history = createMemoryHistory()

test('Should render a Main Layout', () => {

  render(
    <Router history={history}><MainLayout>children</MainLayout></Router>
  );

});
