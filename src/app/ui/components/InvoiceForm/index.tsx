import "react-datepicker/dist/react-datepicker.css";
import { AppContext } from "app/common/contexts/AppContext";
import { Button } from "app/ui/components/Button";
import { Card } from "app/ui/components/Card";
import { Components } from 'api/gen/client';
import { DateSelector } from "app/ui/components/DateSelector";
import { faBullseye, faLandmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form as FormBS, Modal, Row } from 'react-bootstrap';
import { Form, Formik, FormikErrors } from 'formik';
import { formatDate } from "app/common/utils";
import { FormField } from "app/ui/components/FormField";
import { IIinitialValues, IInvoiceUpdatePayload, IInvoiceCreatePayload } from "./types";
import { IInvoiceLine } from "types";
import { SelectAsync } from "app/ui/components/SelectAync";
import { Title } from "app/ui/components/Title";
import { useApi } from "api";
import { useContext, useState } from "react";
import { useHistory } from 'react-router-dom'


interface IProps {
  initialValues: IIinitialValues;
  isCreating?: boolean;
  isUpdating?: boolean;
  dataInvoice?: {
    idInvoice: number;
    tax: string | null;
    total: string | null;
  };
  onSendData: (data: IInvoiceUpdatePayload | IInvoiceCreatePayload) => void;
}

export const EMPTY_INVOICE_LINE: any = {
  id: 0,
  invoice_id: 0,
  product_id: 0,
  quantity: 0,
  label: '',
  unit: 'hour',
  vat_rate: '0',
  price: '0',
  tax: '0',
  product: {
    id: 0,
    label: '',
    vat_rate: '0',
    unit: 'hour',
    unit_price: '0',
    unit_price_without_tax: '0',
    unit_tax: '0'
  }
};

export const InvoiceForm = ({ initialValues, isCreating, isUpdating, dataInvoice, onSendData }: IProps) => {
  let history = useHistory();
  const api = useApi();
  const { showToast } = useContext(AppContext)
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <Title title={`${isCreating ? 'Create' : 'Update'} invoice ${dataInvoice?.idInvoice ?? ''}`} />
      <>
        <Formik
          initialValues={initialValues}
          validate={values => {
            let errors: FormikErrors<IIinitialValues> = {};

            if (isCreating && !values.customer) {
              errors.customer = 'Required';
            }
            if (isUpdating && !values.customer_id) {
              errors.customer_id = 'Required';
            }
            if (!values.date) {
              errors.date = "Required";
            }
            return errors;
          }}
          onSubmit={async (
            values: IIinitialValues,
          ) => {
            const { finalized, paid, customer, deadline, invoice_lines, date } = values;
            const valuesCustomer = (customer as any);

            if (invoice_lines.length === 0) {
              showToast('You should add invoice lines before continuing.', true);
              return;
            }

            const requestInvoice: IInvoiceUpdatePayload | IInvoiceCreatePayload = {
              invoice: {
                ...(isUpdating && { id: dataInvoice?.idInvoice }),
                customer_id: valuesCustomer.value,
                finalized,
                paid,
                date: formatDate(date.toString()),
                deadline,
                invoice_lines_attributes: invoice_lines.map((line: Components.Schemas.InvoiceLine) => {

                  return ({
                    ...line,
                    product_id: isCreating ? (line as unknown as IInvoiceLine).product.value : (line as unknown as IInvoiceLine).product.value || line.product.id,
                  })
                })
              }
            }
            try {
              if (isCreating && requestInvoice && requestInvoice.invoice.invoice_lines_attributes) {
                requestInvoice.invoice.invoice_lines_attributes.map((line: Components.Schemas.InvoiceLineCreatePayload) => {
                  delete (line as any).id;
                  delete (line as any).invoice_id;
                  delete (line as any).product;
                  return line;
                })

              }
              onSendData(requestInvoice);
            } catch (err: any) {
              const { message } = err.response.data;
              showToast(message, true);
            }
          }}
        >
          {({ isSubmitting, handleChange, values, setValues, errors, touched }) => {
            const removeInvoiceLine = (index: number) => {
              const clonedInvoiceLines = [...values.invoice_lines];
              clonedInvoiceLines.splice(index, 1);
              setValues({
                ...values,
                invoice_lines: clonedInvoiceLines
              })
            }

            const addInvoiceLine = () => {
              setValues({
                ...values,
                invoice_lines: [
                  ...values.invoice_lines,
                  EMPTY_INVOICE_LINE
                ]
              })
            }
            return (
              <Form className="formik-form" id="formik">

                <div className="group-form">
                  <h3 className="title-form"> Invoice data</h3>
                  <FormBS.Group as={Row}>
                    <FormField label="Customer" error={isCreating ? errors.customer : errors.customer_id} touched={isCreating ? touched.customer : touched.customer_id}>
                      <SelectAsync
                        {...(isUpdating && { disabled: initialValues.finalized })}
                        {...(isUpdating && { value: initialValues.customer_id })}
                        {...(isUpdating && { label: `${(initialValues.customer as Components.Schemas.Customer).first_name} ${(initialValues.customer as Components.Schemas.Customer).last_name}` })}

                        name="customer" loadAsync={api.getSearchCustomers} keyApi="customers" selectors={["first_name", "last_name"]} />
                    </FormField>

                  </FormBS.Group>

                  <FormBS.Group as={Row}>
                    <FormField smCol="4" label="Creation Date" error={errors.date} touched={touched.date}>
                      <DateSelector
                        {...(isUpdating && { disabled: initialValues.finalized })}
                        {...(isUpdating && { current: new Date(values.date) })}
                        name="date"
                      />
                    </FormField>
                    <FormField smCol="4" label="Deadline" error={errors.deadline} touched={touched.deadline}>
                      <DateSelector
                        {...(isUpdating && { disabled: initialValues.finalized })}
                        {...(isUpdating && values.deadline && { current: new Date(values.deadline) })}
                        name="deadline"
                      />
                    </FormField>
                  </FormBS.Group>

                  <FormBS.Group as={Row}>
                    <FormField label="Status" >
                      <FormBS.Check id="paid" checked={values.paid} label="Paid" name="paid" onChange={handleChange} />
                      <FormBS.Check
                        {...(isUpdating && { disabled: initialValues.finalized })}
                        {...(isUpdating && { checked: initialValues.finalized })}
                        id="finalized" label="Finalized" name="finalized" onChange={handleChange} />
                    </FormField>
                  </FormBS.Group>
                </div>

                <div className="group-form">
                  <h3 className="title-form"> Invoice Lines</h3>
                  {!values.finalized && <Button secondary text='Add new line' onClick={() => addInvoiceLine()} />}
                  <hr></hr>
                  {values.invoice_lines.map((line: Components.Schemas.InvoiceLine, index: number) => (
                    <>

                      <FormBS.Group as={Row}>
                        <FormField label="Label">
                          <FormBS.Control
                            {...(isUpdating && { disabled: initialValues.finalized })}
                            value={line.label}
                            name={`invoice_lines[${index}]['label']`} placeholder="Enter a label for the line" onChange={handleChange} />
                        </FormField>
                      </FormBS.Group>
                      <FormBS.Group as={Row}>
                        <FormField label="Product" smCol="4" >
                          <SelectAsync
                            {...(isUpdating && { disabled: initialValues.finalized })}
                            value={line.product_id} label={line.product.label}
                            name={`invoice_lines[${index}]['product']`}
                            loadAsync={api.getSearchProducts}
                            keyApi="products"
                            selectors={["label"]} />
                        </FormField>
                        <FormField label="Quantity" sm="1" smCol="2" >
                          <FormBS.Control
                            {...(isUpdating && { disabled: initialValues.finalized })}
                            value={line.quantity}
                            name={`invoice_lines[${index}]['quantity']`}
                            placeholder="Enter a value"
                            onChange={handleChange} />
                        </FormField>
                        <FormField label="Unit" sm="1" smCol="2" >
                          <FormBS.Select {...(isUpdating && { disabled: initialValues.finalized })} value={line.unit} name={`invoice_lines[${index}]['unit']`} onChange={handleChange}>
                            <option>Select a value</option>
                            <option value="hour">Hour</option>
                            <option value="day">day</option>
                            <option value="piece">Piece</option>
                          </FormBS.Select>
                        </FormField>
                      </FormBS.Group>
                      <FormBS.Group as={Row}>
                        <FormField label="Vat rate" smCol="2" >
                          <FormBS.Select {...(isUpdating && { disabled: initialValues.finalized })} value={line.vat_rate} name={`invoice_lines[${index}]['vat_rate']`} onChange={handleChange}>
                            <option>Select a value</option>
                            <option value="0">0</option>
                            <option value="5.5">5.5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                          </FormBS.Select>
                        </FormField>
                        <FormField label="Price" smCol="2" >
                          <FormBS.Control {...(isUpdating && { disabled: initialValues.finalized })} value={line.price} name={`invoice_lines[${index}]['price']`} placeholder="Enter a value" onChange={handleChange} />
                        </FormField>
                        <FormField label="Tax" smCol="2" >
                          <FormBS.Control disabled value={line.tax} name={`invoice_lines[${index}]['tax']`} placeholder="Enter a value" onChange={handleChange} />
                        </FormField>
                      </FormBS.Group>
                      {!initialValues.finalized && (
                        <div className="align right">
                          <Button small secondary danger text="Remove this line" onClick={() => removeInvoiceLine(index)} />
                        </div>
                      )}
                      <hr></hr>
                    </>
                  ))}
                </div>
                {isUpdating && dataInvoice && (
                  <div className="group-form flex">
                    <Card icon={<FontAwesomeIcon fixedWidth icon={faLandmark} />} title="Taxes" value={dataInvoice.tax} />
                    <Card icon={<FontAwesomeIcon fixedWidth icon={faBullseye} />} title="Total" value={dataInvoice.total} />
                  </div>
                )}

                <div className="align right">
                  <Button secondary text="Cancel" disabled={isSubmitting} onClick={() => setShowModal(true)} />
                  <Button text={`${isCreating ? 'Create' : 'Update'} invoice`} disabled={isSubmitting} submit />
                </div>
              </Form>
            )
          }}

        </Formik>
        <Modal show={showModal} >
          <Modal.Header closeButton={false}>
            <Modal.Title>Exit</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure you want to cancel the {isCreating ? 'creation' : 'update'}?</Modal.Body>
          <Modal.Footer>
            <Button secondary onClick={() => setShowModal(false)} text="No" />
            <Button onClick={() => history.push('/invoices')} text="Yes" />
          </Modal.Footer>
        </Modal>
      </>
    </>)
}