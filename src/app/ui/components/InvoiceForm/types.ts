import { Components } from "api/gen/client";

export interface IIinitialValues {
  customer: string | Components.Schemas.Customer;
  customer_id: number;
  deadline: string;
  date: string;
  finalized: boolean;
  paid: boolean;
  invoice_lines: Components.Schemas.InvoiceLine[];
}

export interface IInvoiceUpdatePayload {
  invoice: Components.Schemas.InvoiceUpdatePayload;
}
export interface IInvoiceCreatePayload {
  invoice: Components.Schemas.InvoiceCreatePayload;
}