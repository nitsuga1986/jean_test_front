import './styles.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBookmark, faUsers, faArchive } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
import { useLocation } from 'react-router-dom';


interface IProps {
  customers_amount: number;
  products_amount: number;
  invoices_amount: number;
}


export const Sidenav = ({ customers_amount, products_amount, invoices_amount, ...props }: IProps): JSX.Element => {
  const location = useLocation()
  return (
    <aside>
      <i>invoices</i>

      <Link to="/invoices">
        <span className={location.pathname === '/invoices' || location.pathname === '/' ? 'active' : ''}>
          <FontAwesomeIcon fixedWidth icon={faBookmark} />
          All invoices
          <em>{invoices_amount}</em>
        </span>
      </Link>
      <Link to="/customers">
        <span className={location.pathname === '/customers' ? 'active' : ''}>
          <FontAwesomeIcon fixedWidth icon={faUsers} />
          Customers
          <em>{customers_amount}</em>
        </span>
      </Link>

      <Link to="/products">
        <span className={location.pathname === '/products' ? 'active' : ''}>
          <FontAwesomeIcon fixedWidth icon={faArchive} />
          Products
          <em>{products_amount}</em>
        </span>
      </Link>
    </aside>
  )
}