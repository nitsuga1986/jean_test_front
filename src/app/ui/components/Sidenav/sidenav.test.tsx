import { getByText, render } from '@testing-library/react';
import { Sidenav } from './';
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'

test('Should render a Sidenav', () => {
  const history = createMemoryHistory()

  const { container } = render(
    <Router history={history}>
      <Sidenav customers_amount={1} products_amount={2} invoices_amount={3} />
    </Router>
  );

  const invoices = getByText(container, 'All invoices')
  expect(invoices.querySelector('em')?.innerHTML).toBe('3')

  const customers = getByText(container, 'Customers')
  expect(customers.querySelector('em')?.innerHTML).toBe('1')


  const products = getByText(container, 'Products')
  expect(products.querySelector('em')?.innerHTML).toBe('2')

});
