import { Components } from 'api/gen/client';
import { useMemo } from 'react';
import { Pagination } from 'react-bootstrap';
import { useTable } from 'react-table'
import './styles.css';

export interface IValueTable {
  [key: string]: string | number;
}


interface IColumn {
  Header: string
  accessor: string;
}
interface IProps {
  columns: IColumn[];
  data: any[];
  pagination: Components.Schemas.Pagination;
  onClickPage: (page: number) => void;
}

export const Table = ({ data, columns, pagination, onClickPage }: IProps) => {
  let active = pagination.page;
  let items = [];
  for (let number = 1; number <= pagination.total_pages; number++) {
    items.push(
      <Pagination.Item activeLabel='' key={number} active={number === active} onClick={() => { onClickPage(number) }}>
        {number}
      </Pagination.Item >,
    );
  }

  const valueData = useMemo<IValueTable[]>(
    () => data,
    [data]
  )

  const valueColumns = useMemo<IColumn[]>(
    () => columns,
    [columns]
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({ columns: valueColumns, data: valueData })

  return (
    <div className="container-table">
      <div className="tableWrap">
        <table {...getTableProps()} >
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()}>
                    {column.render('Header')}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map(row => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()}>
                        {cell.render('Cell')}
                      </td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        {pagination.total_pages > 1 && <Pagination size="sm">{items}</Pagination>}
      </div>
    </div>
  )
}