import { useState } from 'react';
import './styles.css';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '../Button';

interface IProps {
  onFilter: (value: string) => void;
}

export const Search = ({ onFilter }: IProps) => {
  const [text, setText] = useState('')

  const onClearText = () => {
    const EMPTY_TEXT = '';
    setText(EMPTY_TEXT);
    onFilter(EMPTY_TEXT)
  }
  return (
    <div className="search-wrapper">
      <div className="input-container">
        <FontAwesomeIcon fixedWidth icon={faSearch} />
        <input onKeyDown={(e) => e.key === 'Enter' && onFilter(text)} type="text" value={text} onChange={(event) => setText(event.target.value)} placeholder="Enter your query" />
        <Button secondary onClick={() => onClearText()} text="Clear" />
        <Button onClick={() => onFilter(text)} text="Filter" />
      </div>

    </div>
  );
}