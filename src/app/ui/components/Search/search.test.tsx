import { fireEvent, getByPlaceholderText, getByRole, getByText, render } from '@testing-library/react';
import { Search } from './';

test('Should render a Search and writes on input', () => {
  const handleOnFilter = jest.fn();
  const { container } = render(
    <Search onFilter={handleOnFilter} />
  );
  const input = getByPlaceholderText(container, 'Enter your query')
  fireEvent.change(input, { target: { value: 'Message' } })

  expect(input.value).toBe('Message')
});

test('Should render a Search with a search', () => {
  const handleOnFilter = jest.fn();
  const { container } = render(
    <Search onFilter={handleOnFilter} />
  );

  const button = getByText(container, 'Filter')
  fireEvent.click(button)
  expect(handleOnFilter).toHaveBeenCalledTimes(1)
});

test('Should render a Search and clear', () => {
  const handleOnFilter = jest.fn();
  const { container } = render(
    <Search onFilter={handleOnFilter} />
  );
  const input = getByPlaceholderText(container, 'Enter your query')
  fireEvent.change(input, { target: { value: 'Message' } })
  expect(input.value).toBe('Message')

  const button = getByText(container, 'Clear')
  fireEvent.click(button)
  expect(input.value).toBe('')
});


