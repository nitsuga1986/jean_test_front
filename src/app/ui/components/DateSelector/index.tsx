import { useState } from 'react';
import { useField } from "formik";
import Datepicker from 'react-datepicker';
import { formatDate } from '../../../common/utils';
interface IProps {
  name: string;
  current?: Date;
  disabled?: boolean;
}


export const DateSelector = ({ name, current, disabled = false }: IProps) => {
  const [value, setValue] = useState(current);
  const [field, meta, helpers] = useField(name);


  return (
    <Datepicker disabled={disabled} placeholderText="Select a date" dateFormat="yyyy-MM-dd" selected={value} onChange={(e: any) => {
      setValue(e);
      e ? helpers.setValue(formatDate(e.toString())) : helpers.setValue('');
    }} />
  );
};
