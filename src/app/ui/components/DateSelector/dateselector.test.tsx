import React from 'react';
import { render } from '@testing-library/react';
import { DateSelector } from './';
import { Formik } from 'formik';


test('Should render a date selector', () => {
  render(
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
      }}
      onSubmit={() => { }}
    >
      <DateSelector name="name" />
    </Formik>
  );

});
