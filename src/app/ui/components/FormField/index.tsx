import { Form, Col } from 'react-bootstrap';
import './styles.css';

interface IProps {
  label: string;
  error?: string;
  touched?: boolean;
  sm?: any;
  smCol?: any;
  children: React.ReactNode;
}

export const FormField = ({ label, error, sm = "2", smCol = "10", touched, children }: IProps) => {
  return (
    <>
      <Form.Label column sm={sm}>
        {label}
      </Form.Label>
      <Col sm={smCol}>
        {children}
        <div className="error" >{error && touched ? <em>{error}</em> : null}</div>
      </Col>
    </>
  )
}