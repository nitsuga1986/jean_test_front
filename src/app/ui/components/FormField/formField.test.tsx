import React from 'react';
import { render } from '@testing-library/react';
import { FormField } from './';


test('Should render a Form Field', () => {
  const LABEL = 'MY LABEL'
  render(

    <FormField label={LABEL}>
      <div>children</div>
    </FormField>
  );

});


test('Should render a Form Field with an error', () => {
  const LABEL = 'MY LABEL'
  render(

    <FormField label={LABEL} error="error" touched>
      <div>children</div>
    </FormField>

  );

});
