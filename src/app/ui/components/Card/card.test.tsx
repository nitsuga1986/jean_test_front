import React from 'react';
import { render, screen } from '@testing-library/react';
import { Card } from './';

test('Should render a card with a text passed by params', () => {
  const TEXT = 'Hello world';
  render(<Card icon={<i>icon</i>} value={TEXT} />);

  screen.getByText(TEXT);
});

test('Should render a card with a Title passed by params', () => {
  const TEXT = 'Hello world';
  render(<Card icon={<i>icon</i>} title={TEXT} value="value" />);

  screen.getByText(TEXT);
});

test('Should render a card with a component as icon', () => {
  const TEXT = 'Hello world';
  render(<Card icon={<i data-testid='icon'>icon</i>} title={TEXT} value="value" />);
  screen.getByTestId('icon')
});