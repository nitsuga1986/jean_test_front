import './styles.css'

interface IProps {
  icon: JSX.Element;
  title?: string;
  value: string | null;
}


export const Card = ({ icon, title, value }: IProps) => {
  return (
    <div className="container-card">
      {icon}
      <div className="info">
        <span>{title}</span>
        <em>{value}</em>
      </div>
    </div>
  )
}