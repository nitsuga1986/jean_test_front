import { render } from '@testing-library/react';
import { Formik } from 'formik';
import { SelectAsync } from './';

test('Should render a SelectAsync', () => {
  const handleLoad = jest.fn();
  const { debug, container } = render(
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
      }}
      onSubmit={() => { }}
    ><SelectAsync name="customer" loadAsync={handleLoad} keyApi="customers" selectors={["first_name", "last_name"]} /></Formik>
  );
  const input = container.querySelector('input')

});
