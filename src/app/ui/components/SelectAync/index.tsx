import { useField } from "formik";
import { AsyncPaginate } from "react-select-async-paginate";
import { useEffect, useState } from 'react';


interface IProps {
  value?: number;
  label?: string;
  name: string;
  loadAsync: any;
  keyApi: 'customers' | 'products';
  selectors: string[];
  disabled?: boolean;
}


export const SelectAsync = ({ value, label, name, loadAsync, keyApi, selectors, disabled = false }: IProps) => {
  const [field, meta, helpers] = useField(name);
  const [innerValue, setInnerValue] = useState({ value, label })

  useEffect(() => {
    setInnerValue({
      value, label
    })
  }, [value, label]);

  const loadOptions = async (query: string, prevOptions: string) => {
    const { data } = await loadAsync({ query })

    return {
      options: data[keyApi].map((item: any) => {
        return ({
          value: item.id, label: selectors.reduce((acc, inc) => acc += `${item[inc]} `, '')
        })
      }),
      hasMore: false,
    };
  }
  return (
    <div className="async-selector">
      {disabled ?
        (
          <input disabled value={label} />) :
        (<AsyncPaginate
          value={innerValue}
          name={name}
          loadOptions={loadOptions}
          onChange={(e: any) => {
            setInnerValue(e);
            helpers.setValue(e);
          }}
          onBlur={() => helpers.setTouched(true)}
        />)}

    </div>
  );
};
