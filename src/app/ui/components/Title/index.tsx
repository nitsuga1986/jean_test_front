import React from 'react';
import Badge from 'react-bootstrap/Badge';
import './styles.css';

interface IProps {
  title: string;
  amount?: number;
  buttons?: React.ReactNode[];
}

export const Title = ({ title, amount, buttons }: IProps): JSX.Element => {
  return (
    <div className="container-title">
      <h2 className="title">{title}</h2>
      {amount && <Badge bg="success" text="light">{amount} {title.toLowerCase()} found</Badge>}
      {buttons && (
        <div className="container-buttons">
          {buttons?.map((button, index) => <div key={index}>{button}</div>)}
        </div>
      )}
    </div>
  );
}