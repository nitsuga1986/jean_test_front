import { getByRole, getByText, render } from '@testing-library/react';
import { Title } from './';
import { createMemoryHistory } from 'history'
import { Router } from 'react-router-dom'

test('Should render a Title', () => {

  const { container } = render(
    <Title title="My title" />
  );
  getByRole(container, 'heading', { name: 'My title' })


});

test('Should render a Title with a button', () => {
  const TITLE_BUTTON = 'This is a button'
  const { container } = render(
    <Title title="My title" buttons={[<button>{TITLE_BUTTON}</button>]} />
  );
  getByRole(container, 'button', { name: TITLE_BUTTON })


});

test('Should render a Title with an amount', () => {
  const { debug, container } = render(
    <Title title="My title" amount={2} />
  );

  expect(getByText(container, '2 my title found')).toBeDefined()

});