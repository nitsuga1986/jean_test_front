import { render, screen } from '@testing-library/react';
import { Button } from './';

test('Should renders Button with text and as link', () => {
  const BUTTON_TEXT = 'Hello world';

  const { container } = render(<Button text={BUTTON_TEXT} />);
  screen.getByText(BUTTON_TEXT);
  const link = container.querySelector('a');

  expect(link).toBeDefined();
});

test('Should render the button as native button', () => {
  const { container } = render(<Button text="Button Text" submit />);

  const button = container.querySelector('button');
  expect(button).toBeDefined();
})

test('Should render the button as secondary', () => {
  const { container } = render(<Button text="Button Text" secondary />);

  const button = container.querySelector('.secondary');
  expect(button).toBeDefined();
})

test('Should render the button as small', () => {
  const { container } = render(<Button text="Button Text" small />);

  const button = container.querySelector('.small');
  expect(button).toBeDefined();
})

test('Should render the button as danger', () => {
  const { container } = render(<Button text="Button Text" danger />);

  const button = container.querySelector('.danger');
  expect(button).toBeDefined();
})

test('Should render the button as noMargin', () => {
  const { container } = render(<Button text="Button Text" noMargin />);

  const button = container.querySelector('.noMargin');
  expect(button).toBeDefined();
})