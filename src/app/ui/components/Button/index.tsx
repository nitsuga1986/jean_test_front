import { ButtonHTMLAttributes } from "react";

import './styles.css';

interface IProps extends ButtonHTMLAttributes<HTMLAnchorElement | HTMLButtonElement> {
  text: string | React.ReactNode;
  secondary?: boolean;
  small?: boolean;
  danger?: boolean;
  submit?: boolean;
  noMargin?: boolean;
}

export const Button = ({ text, secondary, small, danger, submit, noMargin, ...props }: IProps) => {
  let classname = 'sui-button';

  if (secondary) classname += ' secondary';
  if (small) classname += ' small';
  if (danger) classname += ' danger';
  if (noMargin) classname += ' noMargin';

  if (submit) {
    return <button type="submit" className={classname}  {...props} >{text}</button>
  } else {
    return <a role="button" className={classname}  {...props} >{text}</a>
  }

}