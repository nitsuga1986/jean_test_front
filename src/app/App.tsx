import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { MainLayout } from './ui/layout/main.layout';
import { routes } from './common/routes';
import { IRoute, ViewProps } from 'types';
import AppProvider from './common/contexts/AppContext';

function App() {

  return (
    <AppProvider>
      <Router>
        <MainLayout>
          <Switch>
            {routes.map(({ view: View, key, ...route }: IRoute) => {
              return <Route exact key={key} render={(props: ViewProps) => <View {...props} />}  {...route} />
            })}

          </Switch>
        </MainLayout>

      </Router>
    </AppProvider>
  );
}

export default App;

