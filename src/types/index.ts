import { OperationMethods } from "api/gen/client";
import { Awaited } from "./helpers";
import { RouteComponentProps, RouteProps } from 'react-router';

export type Invoice = Awaited<
  ReturnType<OperationMethods["getInvoices"]>
>["data"]["invoices"][0];

export interface IRoute extends RouteProps {
  key: string;
  view: React.FC<any>;
}

export interface ViewProps<T = any> extends RouteComponentProps<T> {}


export interface IProduct {
  value: number;
  label: string;
  
}
export interface IInvoiceLine {
  id?: number;
  invoice_id?: number;
  quantity: number;
  label: string;
  unit: string;
  vat_rate: string;
  price: string;
  tax: string;
  product: IProduct;
}

export interface IInvoice {
  id: number;
  customer_id: number;
  finalized: boolean;
  paid: boolean;
  date:string;
  deadline:string;
  total:string;
  tax:string;
  invoice_lines: IInvoiceLine[];
}